﻿namespace FormTutorial
{
    partial class Multiplayer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.IpBox = new System.Windows.Forms.TextBox();
            this.Connect = new System.Windows.Forms.Button();
            this.HostGame = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Connect to game";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "IP: ";
            // 
            // IpBox
            // 
            this.IpBox.Location = new System.Drawing.Point(51, 91);
            this.IpBox.Name = "IpBox";
            this.IpBox.Size = new System.Drawing.Size(222, 27);
            this.IpBox.TabIndex = 2;
            // 
            // Connect
            // 
            this.Connect.Location = new System.Drawing.Point(295, 91);
            this.Connect.Name = "Connect";
            this.Connect.Size = new System.Drawing.Size(94, 29);
            this.Connect.TabIndex = 3;
            this.Connect.Text = "Connect";
            this.Connect.UseVisualStyleBackColor = true;
            this.Connect.Click += new System.EventHandler(this.Connect_Click);
            // 
            // HostGame
            // 
            this.HostGame.Location = new System.Drawing.Point(34, 151);
            this.HostGame.Name = "HostGame";
            this.HostGame.Size = new System.Drawing.Size(326, 51);
            this.HostGame.TabIndex = 4;
            this.HostGame.Text = "Host Game";
            this.HostGame.UseVisualStyleBackColor = true;
            this.HostGame.Click += new System.EventHandler(this.HostGame_Click);
            // 
            // Multiplayer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 244);
            this.Controls.Add(this.HostGame);
            this.Controls.Add(this.Connect);
            this.Controls.Add(this.IpBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Multiplayer";
            this.Text = "Multiplayer";
            this.Load += new System.EventHandler(this.Multiplayer_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private Label label2;
        private TextBox IpBox;
        private Button Connect;
        private Button HostGame;
    }
}