﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml;
using System.Data.SqlClient;

namespace FormTutorial
{
    public partial class AdminSetting : Form
    {
        SqlConnection sqlConnection = new SqlConnection("Data Source=DESKTOP-8HGESQN;Initial Catalog=OOP2SQL;Integrated Security=True");

        public AdminSetting()
        {
            InitializeComponent();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            XDocument xdosya = XDocument.Load("Users.xml");
            XElement element = xdosya.Element("Users").Elements("User").FirstOrDefault(x => x.Element("UserName").Value == textBox1.Text);
            if (element!=null)
            {
                element.SetElementValue("UserName", textBox1.Text);
                element.SetElementValue("Password", textBox2.Text);
                element.SetElementValue("NameSurname", textBox5.Text);
                element.SetElementValue("Number", textBox4.Text);
                element.SetElementValue("Adress", richTextBox1.Text);
                element.SetElementValue("City", textBox8.Text);
                element.SetElementValue("Country", textBox7.Text);
                element.SetElementValue("E-mail", textBox6.Text);
                xdosya.Save("Users.xml");
                MessageBox.Show("Bilgiler güncellendi");
                ReadXmlUser();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            textBox2.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            textBox5.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            textBox4.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            richTextBox1.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            textBox8.Text = dataGridView1.CurrentRow.Cells[5].Value.ToString();
            textBox7.Text = dataGridView1.CurrentRow.Cells[6].Value.ToString();
            textBox6.Text = dataGridView1.CurrentRow.Cells[7].Value.ToString();
        }
        void ReadXmlUser()
        {
            DataSet ds = new DataSet();
            XmlReader xmlReader = XmlReader.Create(@"Users.xml", new XmlReaderSettings());
            ds.ReadXml(xmlReader);            
            dataGridView1.DataSource = ds.Tables[0];
            xmlReader.Close();
        }
        void SqlVeriAl()
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = "Select * From UserTable";
            sqlCommand.Connection = sqlConnection;

            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            DataTable dt = new DataTable();

            sqlDataAdapter.Fill(dt);

            dataGridView1.DataSource = dt;
        }
        void SqlVeriYaz()
        {
            sqlConnection.Open();
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand = new SqlCommand("INSERT INTO UserTable(UserName,Password,NameSurname,Number,Adress,City,Country,Email) VALUES (@UserName,@Password,@NameSurname,@Number,@Adress,@City,@Country,@Email)");
            sqlCommand.Connection = sqlConnection;

           // sqlCommand.Parameters.AddWithValue("@ID",3);
            sqlCommand.Parameters.AddWithValue("@UserName", textBox1.Text);
            sqlCommand.Parameters.AddWithValue("@Password", textBox2.Text);
            sqlCommand.Parameters.AddWithValue("@NameSurname", textBox5.Text);
            sqlCommand.Parameters.AddWithValue("@Number", textBox4.Text);
            sqlCommand.Parameters.AddWithValue("@Adress", textBox1.Text);
            sqlCommand.Parameters.AddWithValue("@City", textBox8.Text);
            sqlCommand.Parameters.AddWithValue("@Country", textBox7.Text);
            sqlCommand.Parameters.AddWithValue("@Email", textBox6.Text);

            
            sqlCommand.ExecuteNonQuery();
            sqlConnection.Close();
            MessageBox.Show("Kullanıcı eklendi");

        }
        private void AdminSetting_Load(object sender, EventArgs e)
        {
            ReadXmlUser();
            SqlVeriAl();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            WriteXml();
            //ReadXmlUser();            
            SqlVeriYaz();
            SqlVeriAl();
        }
        void WriteXml()
        {
            XDocument xDocument = XDocument.Load("Users.xml");
            XElement rootElement = xDocument.Root;
            XElement element = new XElement("User");
            XElement UserName = new XElement("UserName", textBox1.Text);
            XElement Password = new XElement("Password", textBox2.Text);
            XElement NameSurname = new XElement("NameSurname", textBox5.Text);
            XElement PhoneNumber = new XElement("Number", textBox4.Text);
            XElement Address = new XElement("Adress", richTextBox1.Text);
            XElement City = new XElement("City", textBox8.Text);
            XElement Country = new XElement("Country", textBox7.Text);
            XElement Email = new XElement("E-mail", textBox6.Text);
            element.Add(UserName, Password, NameSurname, PhoneNumber, Address, City, Country, Email);
            rootElement.Add(element);
            xDocument.Save(@"Users.xml");
            MessageBox.Show("Sign Up is complete");
        }
        void RemoveElement()
        {
            XDocument xdosya = XDocument.Load("Users.xml");
            xdosya.Root.Elements().Where(x => x.Element("UserName").Value == dataGridView1.CurrentRow.Cells[0].Value.ToString()).Remove();
            xdosya.Save("Users.xml");
            MessageBox.Show("Kayıl silindi");
            ReadXmlUser();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            RemoveElement();
        }
    }
}
