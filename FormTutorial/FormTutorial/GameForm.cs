﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;


namespace FormTutorial
{
    public partial class GameForm : Form
    {
        bool isClicked = false;
        int tmpI, tmpJ;
        static int score=0;
        static char[,] grid = new char[100, 100];
        bool is_itPath = false, isGameOver=false;
        int[,] tilesAdd = new int[20, 20];
        public List<Button> buttonList = new List<Button>();
        List<Button> buttonListTMP = new List<Button>();
        List<QItem> q = new List<QItem>();
        int ROW, COL, size, rightX, rightY, leftX, leftY, name = 0; // medium ise size =60, easy ise size= 36 ,hard ise 90
        string sq = " ", rd = " ", tr = " ", red = " ", blue = " ", black = " ";
        Image r, l;
        Color rc, lc;
        Button[,] buttonArray = new Button[100, 100];
        public GameForm()
        {
            InitializeComponent();
        }
        private void GameForm_Load(object sender, EventArgs e)
        {
            this.TopMost = true;
            ReadSave();
            ButtonOluştur();
            BtnEkle();
            ChangeButtonİmage();
            ChangeButtonColor();                        
        }
        void ButtonOluştur()
        {
            for (int i = 0; i < ROW; i++)
            {
                for (int j = 0; j < COL; j++)
                {
                    Button b = new Button();
                    QItem q = new QItem(ROW, COL, 0);
                    b.BackColor = Color.White;
                    b.Width = size;
                    b.Height = size;
                    b.Tag = "default";
                    b.Location = new Point(size * i, size * j); // satır ve sütünları ayarlama                     
                    b.MouseDown += new MouseEventHandler(buttonDown);
                    buttonArray[j, i] = b;
                    buttonList.Add(b);
                    //Console.Write(buttonArray[i, j].BackColor.ToString());
                }
                //Console.Write("\n");
            }
        }
        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void buttonDown(object sender, MouseEventArgs e)
        {
            List<QItem> QBtn = new List<QItem>();
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                Button bRight = (Button)sender;
                r = bRight.Image;
                rc = bRight.BackColor;
                rightX = bRight.Location.X;
                rightY = bRight.Location.Y;
                //MessageBox.Show(rightX+" "+ rightY);
                isClicked = true;

            }
            if (e.Button == System.Windows.Forms.MouseButtons.Left && isClicked == true)
            {
                Button bLeft = (Button)sender;
                leftX = bLeft.Location.X;
                leftY = bLeft.Location.Y;
                //bLeft.BackColor = rc;
                //bLeft.Image = r;
                isClicked = false;
                CreateGrid();
                ButtonVarMıKontrol();
                if (is_itPath == true)
                {
                    if (minDistance(ref QBtn)) BtnMove(QBtn);
                        else MessageBox.Show("Yol yok!!!");
                }
            }            
        }
        void GameOver()
        {
            int counter=0;
            foreach (Button b in buttonList)
            {
                if (b.BackColor == Color.White)
                {
                    counter++;                    
                }               
            }
            if (counter < 3)
            {
                isGameOver = true;
            }
            if (isGameOver)
            {
                MessageBox.Show("Game Over" + " Your Score is: " + score);
                this.Close();
            }
        }
        void ButtonVarMıKontrol()
        {
            foreach (Button b in buttonList)
            {
                if (b.Location.X == leftX && b.Location.Y == leftY)
                {
                    if (b.BackColor != Color.White)
                    {
                       is_itPath = false;
                    }
                    else is_itPath = true;
                }
            }            
        }
        void BtnMove(List<QItem> list)
        {
            int sleep = 500;
            list.Reverse();
            SoundPlayer soundPlayer = new SoundPlayer("movesound.wav");
            for (int i = 0; i < list.Count - 1; i++)
            {                
                int source = (list[i].col * ROW) + list[i].row;
                int dest = (list[i + 1].col * ROW) + list[i + 1].row;

                buttonList[dest].BackColor = Color.White;

                Task.Delay(sleep).Wait();
                soundPlayer.Play();
                // resimleri atlatma
                buttonList[dest].Image = buttonList[source].Image;
                buttonList[source].Image = default;

                // renkleri atlatma
                buttonList[dest].BackColor = buttonList[source].BackColor;
                buttonList[source].BackColor = Color.White;
                // tagleri atlatma
                buttonList[dest].Tag = buttonList[source].Tag;
                buttonList[source].Tag = "default";
            }
            ChangeButtonİmage();
            ChangeButtonColor();
            DeleteColorImage();
            scoreCheck();
            WriteScore();
            GameOver();
        }        
        void WriteScore()
        {
            label2.Text = score.ToString();
        }
        void scoreCheck()
        {            
            for (int i = 0; i < ROW; i++)
            {
                for (int j = 0; j < COL - 4; j++)
                {
                    if (buttonArray[i,j].BackColor == buttonArray[i,j+1].BackColor 
                        && buttonArray[i,j].BackColor == buttonArray[i,j + 2].BackColor
                        && buttonArray[i,j].BackColor == buttonArray[i,j + 3].BackColor
                        && buttonArray[i,j].BackColor == buttonArray[i,j + 4].BackColor
                        && buttonArray[i, j].Tag == buttonArray[i, j + 1].Tag
                        && buttonArray[i, j].Tag == buttonArray[i, j + 2].Tag
                        && buttonArray[i, j].Tag == buttonArray[i, j + 3].Tag
                        && buttonArray[i, j].Tag == buttonArray[i, j + 4].Tag
                        && buttonArray[i,j].BackColor != Color.White)                        
                    {                        
                        for (int k = j+4; k >= j; k--)
                        {
                            buttonArray[i, k].BackColor = Color.White;
                            buttonArray[i, k].Image = default;
                        }
                        score += 100;
                    }                    
                }
            }
            for (int i = 0; i < ROW-4; i++)
            {
                for (int j = 0; j < COL; j++)
                {
                    if (buttonArray[i, j].BackColor == buttonArray[i+1,j].BackColor
                        && buttonArray[i, j].BackColor == buttonArray[i+2,j].BackColor
                        && buttonArray[i, j].BackColor == buttonArray[i+3,j].BackColor
                        && buttonArray[i, j].BackColor == buttonArray[i+4,j].BackColor
                        && buttonArray[i, j].Tag == buttonArray[i + 1, j].Tag
                        && buttonArray[i, j].Tag == buttonArray[i + 2, j].Tag
                        && buttonArray[i, j].Tag == buttonArray[i + 3, j].Tag
                        && buttonArray[i, j].Tag == buttonArray[i + 4, j].Tag
                        && buttonArray[i, j].BackColor != Color.White)
                    {
                        for (int k = i + 4; k >= i; k--)
                        {
                            buttonArray[k, j].BackColor = Color.White;
                            buttonArray[k, j].Image = default;
                        }
                        score += 100;
                    }
                }
            }
        }    
        void DeleteColorImage()
        {
            foreach (Button b in buttonList)
            {
                if (b.BackColor != Color.White && b.Image == default)
                {
                    b.BackColor = Color.White;
                }
            }
        }
        private void Button_Click(object? sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
        void BtnEkle()
        {
            foreach (Button b in buttonList)
            {
                panel1.Controls.Add(b);
            }
        }        
        void ChangeButtonİmage()
        {
            foreach (Button b in buttonList)
            {
                for (int i = 0; i < 3; i++)
                {
                Hata:;
                    Random random = new Random();
                    int rnd = random.Next(buttonList.Count);
                    int sh = random.Next(3);
                    if (buttonList[rnd].Tag == "square" || buttonList[rnd].Tag == "round" || buttonList[rnd].Tag == "triangle")
                    {
                        goto Hata;
                    }
                    else if (sq == "Square" && sh == 0 && buttonList[rnd].Tag == "default")
                    {
                        buttonList[rnd].Image = Image.FromFile("kare.png"); //şekil ekleme
                        buttonList[rnd].Tag = "square";
                        buttonList[rnd].Name = "square";
                    }
                    else if (rd == "Round" && sh == 1 && buttonList[rnd].Tag == "default")
                    {
                        buttonList[rnd].Image = Image.FromFile("round.png"); //şekil ekleme
                        buttonList[rnd].Tag = "round";
                        buttonList[rnd].Name = "round";
                    }
                    else if (tr == "Triangle" && sh == 2 && buttonList[rnd].Tag == "default")
                    {
                        buttonList[rnd].Image = Image.FromFile("triangle.png"); //şekil ekleme
                        buttonList[rnd].Tag = "triangle";
                        buttonList[rnd].Name = "triangle";
                    }
                    else goto Hata;
                    
                    panel1.Controls.Add(b);
                }
                break;
            }
        }        
        void ChangeButtonColor()
        {
            foreach (Button button in buttonList)
            {

                if (button.Tag == "square" && button.BackColor != Color.Blue
                    && button.BackColor != Color.Black && button.BackColor != Color.Red)
                {
                A:;
                    Random rnd = new Random();
                    int renk = rnd.Next(3);
                    if (renk == 0 && blue == "Blue") button.BackColor = Color.Blue;
                    else if (renk == 1 && red == "Red") button.BackColor = Color.Red;
                    else if (renk == 2 && black == "Black") button.BackColor = Color.Black;
                    else goto A;                   
                }
                else if (button.Tag == "round" && button.BackColor != Color.Blue
                    && button.BackColor != Color.Black && button.BackColor != Color.Red)
                {
                B:;

                    Random rnd = new Random();
                    int renk = rnd.Next(3);
                    if (renk == 0 && blue == "Blue") button.BackColor = Color.Blue;
                    else if (renk == 1 && red == "Red") button.BackColor = Color.Red;
                    else if (renk == 2 && black == "Black") button.BackColor = Color.Black;
                    else goto B;
                }
                else if (button.Tag == "triangle" && button.BackColor != Color.Blue
                    && button.BackColor != Color.Black && button.BackColor != Color.Red)
                {
                C:;

                    Random rnd = new Random();
                    int renk = rnd.Next(3);
                    if (renk == 0 && blue == "Blue") button.BackColor = Color.Blue;
                    else if (renk == 1 && red == "Red") button.BackColor = Color.Red;
                    else if (renk == 2 && black == "Black") button.BackColor = Color.Black;
                    else goto C;
                }               
            }
        }
        void ReadSave()
        {
            string[] vs = new string[] { };
            string words = File.ReadAllText("./save.txt");
            vs = words.Split(" ");//txt'yi boşluklara göre ayırma
            for (int i = 0; i < vs.Length; i++)
            {
                if (vs[i] == "\n")
                {
                    vs[i] = " ";
                }
            }

            if (vs.Contains("Red"))
            {
                red = "Red";
            }
            if (vs.Contains("Blue"))
            {
                blue = "Blue";
            }
            if (vs.Contains("Black"))
            {
                black = "Black";
            }
            if (vs.Contains("Square"))
            {
                sq = "Square";
            }
            if (vs.Contains("Triangle"))
            {
                tr = "Triangle";
            }
            if (vs.Contains("Round"))
            {
                rd = "Round";
            }
            if (vs.Contains("Easy"))
            {
                size = 34;
                ROW = 15;
                COL = 15;
            }
            if (vs.Contains("Medium"))
            {
                size = 34;
                ROW = 9;
                COL = 9;
            }
            if (vs.Contains("Hard"))
            {
                size = 34;
                ROW = 6;
                COL = 6;
            }
            if (vs.Contains("Custom"))
            {

            }
        }
        void CreateGrid()
        {
            
            // Button array içindekileri 0 ve *'lara çevirme
            for (int i = 0; i < ROW; i++)
            {
                for (int j = 0; j < COL; j++)
                {
                    if (buttonArray[i, j].BackColor == Color.White)
                    {
                        grid[i, j] = '*';
                    }
                    if (buttonArray[i, j].BackColor != Color.White) grid[i, j] = '0';
                    if (rightX == buttonArray[i, j].Location.X && rightY == buttonArray[i, j].Location.Y)
                    {
                        grid[i, j] = 's';
                    }
                    if (leftX == buttonArray[i, j].Location.X && leftY == buttonArray[i, j].Location.Y)
                    {
                        grid[i, j] = 'd';
                    }
                    Console.Write(grid[i, j].ToString() + " ");
                    //Console.Write(buttonArray[i, j].BackColor.ToString() + " ");

                }
                Console.Write("\n");
            }
            //return grid;
        }
        class QItem
        {
            public int row;
            public int col;
            public int dist;
            public QItem prev;
            public QItem(int x, int y, int w)
            {
                row = x;
                col = y;  
                dist = w;
                prev = null;
            }
        };
        private bool GridControl(Button btn)
        {
            return btn.Image == null;
        }
        private bool isEmptyTile(Button tile)
        {

            return tile.Image == null;
        }
        bool minDistance(ref List<QItem> arr)
        {
            QItem source = new QItem(0, 0, 0);
            bool[,] visited = new bool[ROW, COL];
            for (int i = 0; i < ROW; i++)
            {
                for (int j = 0; j < COL; j++)
                {
                    if (grid[i, j] == '0')
                    {
                        visited[i, j] = true;
                    }
                    else
                    {
                        visited[i, j] = false;
                    }

                    if (grid[i, j] == 's')
                    {
                        source.row = i;
                        source.col = j;
                    }
                    Console.Write(visited[i, j].ToString() + " ");
                }
                Console.Write("\n");
            }
            //Console.Write(source.row + " " + source.col);

            List<QItem> QBtn = new List<QItem>();
            QBtn.Add(source);
            visited[source.row, source.col] = true;
            while (QBtn.Any())
            {
                QItem p = QBtn[0];
                QBtn.RemoveAt(0);

                if (grid[p.row, p.col] == 'd')
                {
                    while (p != null)
                    {
                        arr.Add(p);
                        p = p.prev;
                    }
                    return true;
                }
                if (p.row - 1 >= 0 && visited[p.row - 1, p.col] == false)
                {
                    QItem q = new QItem(p.row - 1, p.col, p.dist + 1);
                    q.prev = p;
                    QBtn.Add(q);
                    visited[p.row - 1, p.col] = true;
                }
                if (p.row + 1 < ROW && visited[p.row + 1, p.col] == false)
                {
                    QItem q = new QItem(p.row + 1, p.col, p.dist + 1);
                    q.prev = p;
                    QBtn.Add(q);
                    visited[p.row + 1, p.col] = true;
                }
                if (p.col - 1 >= 0 && visited[p.row, p.col - 1] == false)
                {
                    QItem q = new QItem(p.row, p.col - 1, p.dist + 1);
                    q.prev = p;
                    QBtn.Add(q);
                    visited[p.row, p.col - 1] = true;
                }
                if (p.col + 1 < COL && visited[p.row, p.col + 1] == false)
                {
                    QItem q = new QItem(p.row, p.col + 1, p.dist + 1);
                    q.prev = p;
                    QBtn.Add(q);
                    visited[p.row, p.col + 1] = true;
                }
                //Console.Write(list[0].row + " " + list[0].col);
            }
            Console.Write(QBtn.Count);
            return false;
        }
    }
}   

