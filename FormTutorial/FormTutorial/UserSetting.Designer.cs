﻿namespace FormTutorial
{
    partial class UserSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.UserNameTxtBox = new System.Windows.Forms.TextBox();
            this.PasswordTxtBox = new System.Windows.Forms.TextBox();
            this.EmailTxtBox = new System.Windows.Forms.TextBox();
            this.CountryTxtBox = new System.Windows.Forms.TextBox();
            this.CityTxtBox = new System.Windows.Forms.TextBox();
            this.PhoneTxtBox = new System.Windows.Forms.MaskedTextBox();
            this.AdressTxtBox = new System.Windows.Forms.RichTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.NameTxtBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Name-Surname";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Phone Number";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(60, 217);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Address";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(84, 357);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "City";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(59, 402);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "Country";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(69, 447);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 20);
            this.label8.TabIndex = 7;
            this.label8.Text = "E-mail";
            // 
            // UserNameTxtBox
            // 
            this.UserNameTxtBox.Location = new System.Drawing.Point(135, 34);
            this.UserNameTxtBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UserNameTxtBox.Name = "UserNameTxtBox";
            this.UserNameTxtBox.Size = new System.Drawing.Size(275, 27);
            this.UserNameTxtBox.TabIndex = 8;
            // 
            // PasswordTxtBox
            // 
            this.PasswordTxtBox.Location = new System.Drawing.Point(135, 79);
            this.PasswordTxtBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PasswordTxtBox.Name = "PasswordTxtBox";
            this.PasswordTxtBox.Size = new System.Drawing.Size(275, 27);
            this.PasswordTxtBox.TabIndex = 9;
            // 
            // EmailTxtBox
            // 
            this.EmailTxtBox.Location = new System.Drawing.Point(135, 443);
            this.EmailTxtBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.EmailTxtBox.Name = "EmailTxtBox";
            this.EmailTxtBox.Size = new System.Drawing.Size(275, 27);
            this.EmailTxtBox.TabIndex = 10;
            // 
            // CountryTxtBox
            // 
            this.CountryTxtBox.Location = new System.Drawing.Point(135, 398);
            this.CountryTxtBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CountryTxtBox.Name = "CountryTxtBox";
            this.CountryTxtBox.Size = new System.Drawing.Size(275, 27);
            this.CountryTxtBox.TabIndex = 11;
            // 
            // CityTxtBox
            // 
            this.CityTxtBox.Location = new System.Drawing.Point(135, 353);
            this.CityTxtBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CityTxtBox.Name = "CityTxtBox";
            this.CityTxtBox.Size = new System.Drawing.Size(275, 27);
            this.CityTxtBox.TabIndex = 12;
            // 
            // PhoneTxtBox
            // 
            this.PhoneTxtBox.Location = new System.Drawing.Point(135, 170);
            this.PhoneTxtBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PhoneTxtBox.Mask = "(999) 000-0000";
            this.PhoneTxtBox.Name = "PhoneTxtBox";
            this.PhoneTxtBox.Size = new System.Drawing.Size(275, 27);
            this.PhoneTxtBox.TabIndex = 15;
            // 
            // AdressTxtBox
            // 
            this.AdressTxtBox.Location = new System.Drawing.Point(135, 215);
            this.AdressTxtBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.AdressTxtBox.Name = "AdressTxtBox";
            this.AdressTxtBox.Size = new System.Drawing.Size(275, 121);
            this.AdressTxtBox.TabIndex = 16;
            this.AdressTxtBox.Text = "";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(192, 520);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(119, 39);
            this.button1.TabIndex = 17;
            this.button1.Text = "Register";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // NameTxtBox
            // 
            this.NameTxtBox.Location = new System.Drawing.Point(135, 126);
            this.NameTxtBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NameTxtBox.Name = "NameTxtBox";
            this.NameTxtBox.Size = new System.Drawing.Size(275, 27);
            this.NameTxtBox.TabIndex = 18;
            // 
            // UserSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 572);
            this.Controls.Add(this.NameTxtBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.AdressTxtBox);
            this.Controls.Add(this.PhoneTxtBox);
            this.Controls.Add(this.CityTxtBox);
            this.Controls.Add(this.CountryTxtBox);
            this.Controls.Add(this.EmailTxtBox);
            this.Controls.Add(this.PasswordTxtBox);
            this.Controls.Add(this.UserNameTxtBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "UserSetting";
            this.Text = "UserSetting";
            this.Load += new System.EventHandler(this.UserSetting_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private TextBox UserNameTxtBox;
        private TextBox PasswordTxtBox;
        private TextBox EmailTxtBox;
        private TextBox CountryTxtBox;
        private TextBox CityTxtBox;
        private MaskedTextBox PhoneTxtBox;
        private RichTextBox AdressTxtBox;
        private Button button1;
        private TextBox NameTxtBox;
    }
}