﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace FormTutorial
{
    public partial class UserManage : Form
    {
        public UserManage()
        {
            InitializeComponent();
        }

        private void UserManage_Load(object sender, EventArgs e)
        {
            StreamReader streamReader = new StreamReader("usersave.txt");

            textBox1.Text = streamReader.ReadLine();
            streamReader.Close();

            XmlDocument doc = new XmlDocument();
            doc.Load("Users.xml");
            foreach (XmlNode node in doc.DocumentElement)
            {
                if (textBox1.Text==node["UserName"].InnerText)
                {
                    textBox1.Text = node["UserName"].InnerText;
                    textBox2.Text = node["Password"].InnerText;
                    textBox6.Text = node["NameSurname"].InnerText;
                    maskedTextBox1.Text = node["Number"].InnerText;
                    richTextBox1.Text = node["Adress"].InnerText;
                    textBox5.Text = node["City"].InnerText;
                    textBox4.Text = node["Country"].InnerText;
                    textBox3.Text = node["E-mail"].InnerText;
                }                
            }


            ReadXmlUser();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            XDocument xdosya = XDocument.Load("Users.xml");
            XElement element = xdosya.Element("Users").Elements("User").FirstOrDefault(x => x.Element("UserName").Value == textBox1.Text);
            if (element != null)
            {
                element.SetElementValue("UserName", textBox1.Text);
                element.SetElementValue("Password", textBox2.Text);
                element.SetElementValue("NameSurname", textBox6.Text);
                element.SetElementValue("Number", maskedTextBox1.Text);
                element.SetElementValue("Adress", richTextBox1.Text);
                element.SetElementValue("City", textBox5.Text);
                element.SetElementValue("Country", textBox4.Text);
                element.SetElementValue("E-mail", textBox3.Text);
                xdosya.Save("Users.xml");
                ReadXmlUser();
                MessageBox.Show("Bilgiler güncellendi");                
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //textBox1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            //textBox2.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            //textBox6.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            //maskedTextBox1.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            //richTextBox1.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            //textBox5.Text = dataGridView1.CurrentRow.Cells[5].Value.ToString();
            //textBox4.Text = dataGridView1.CurrentRow.Cells[6].Value.ToString();
            //textBox3.Text = dataGridView1.CurrentRow.Cells[7].Value.ToString();            
        }
        void ReadXmlUser()
        {
            DataSet ds = new DataSet();
            XmlReader xmlReader = XmlReader.Create(@"Users.xml", new XmlReaderSettings());
            ds.ReadXml(xmlReader);
            //dataGridView1.DataSource = ds.Tables[0];
            xmlReader.Close();
        }
    }
}
