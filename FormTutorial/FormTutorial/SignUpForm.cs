﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace FormTutorial
{
    public partial class SignUpForm : Form
    {
        public SignUpForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void SignUpForm_Load(object sender, EventArgs e)
        {
            if (File.Exists(@"Users.xml"))
            {

            }
            else 
                CreateXmlUser(); 
        }
        void CreateXmlUser()
        {
            XmlTextWriter xmlTextWriter = new XmlTextWriter(@"Users.xml",Encoding.UTF8);
            xmlTextWriter.Formatting = Formatting.Indented;
            xmlTextWriter.WriteStartDocument();
            xmlTextWriter.WriteStartElement("Users");
            xmlTextWriter.WriteEndElement();
            xmlTextWriter.Close();
        }
      
        void WriteXmlUsers()
        {
            XDocument xDocument = XDocument.Load(@"Users.xml");
            XElement rootElement = xDocument.Root;
            XElement element = new XElement("User");
            XElement UserName = new XElement("UserName", UserNameTxtBox.Text);
            XElement Password = new XElement("Password", PasswordTxtBox.Text);
            XElement NameSurname = new XElement("NameSurname", NameTxtBox.Text);
            XElement PhoneNumber = new XElement("Number", PhoneNumberTxtBox.Text);
            XElement Address = new XElement("Adress", AdressTxtBox.Text);
            XElement City = new XElement("City", CityTxtBox.Text);
            XElement Country = new XElement("Country", CountryTxtBox.Text);
            XElement Email = new XElement("E-mail", EmailTxtBox.Text);
            element.Add(UserName, Password, NameSurname, PhoneNumber, Address, City, Country, Email);
            rootElement.Add(element);
            element.Remove();
            xDocument.Save(@"Users.xml");
            MessageBox.Show("Sign Up is complete");
        }
       
        void ReadXml()
        {
            DataSet ds = new DataSet();
            XmlReader xmlReader = XmlReader.Create(@"Users.xml",new XmlReaderSettings());
            ds.ReadXml(xmlReader);
            DataGridView dataGridView = new DataGridView();
            dataGridView.Show();
            dataGridView.DataSource = ds.Tables[0];
            xmlReader.Close();
        }
        
    }
}
