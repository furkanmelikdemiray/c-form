﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormTutorial
{
    public partial class SettingsMenu : Form
    {
        string shapeSq,shapeTr,shapeRo,colorRd,colorBlue,colorBlack;
        string difficulty;
        static int  x,y;
       // StreamReader streamReader = new StreamReader("./save.txt");
          

        string[] vs = new string[] { };
        public SettingsMenu()
        {            
            InitializeComponent();
            textBoxX.Enabled = false;
            textBoxY.Enabled = false;            
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
        
        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {          
            textBoxX.Enabled = radioButton4.Checked;
            textBoxY.Enabled = radioButton4.Checked;
            if (radioButton4.Checked==true)
            {                
                difficulty = "Custom";               
            }           
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked==true)
            {
                difficulty = "Easy";   
                
            }          
                
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked == true)
            {
                difficulty = "Medium";               
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked == true)
            {
                difficulty = "Hard";                
            }
        }

        private void label1_TextChanged(object sender, EventArgs e)
        {

        }       

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBoxX.Text == string.Empty && textBoxY.Text == string.Empty && radioButton4.Checked == false)
            {
                MessageBox.Show("Saved");
            }
            else
            {
                try // Matrix değerleri integer ? kontrol
                {
                    x = int.Parse(textBoxX.Text);
                    y = int.Parse(textBoxY.Text);                    
                }
                catch
                {
                    MessageBox.Show("Pls enter a number!!!");                    
                }
                MessageBox.Show("Saved");
                int[,] m = new int[x, y];
            }                 
                        
            TextWriter textWriter = new StreamWriter("./save.txt", false);  // Save txt oluşturma         
            

            if (difficulty=="Custom")
            {
                if (x==0 || y==0)
                {

                }
                else
                    textWriter.WriteLine("Difficulty and Matrix: " + difficulty+" "+" m["+x+"]"+"["+y+"]" + "\nShape Selected: " + shapeSq + " " + shapeTr + " " + shapeRo + " " + "\nColors: " + colorRd + " " + colorBlue + " " + colorBlack + " ");
            }
            else
                textWriter.WriteLine("Difficulty: " + difficulty+" " + "\nShape Selected: " + shapeSq+" "+shapeTr+" "+shapeRo+" "+ "\nColors: "+colorRd+" "+colorBlue+" "+colorBlack+" " );            
            
            textWriter.Close();                 

        }       

        private void groupBox2_Enter(object sender, EventArgs e)
        {
            
        }

        private void SettingsMenu_Load(object sender, EventArgs e)
        {
            string words = File.ReadAllText("./save.txt");
            vs = words.Split(" ");
            for (int i = 0; i < vs.Length; i++)
            {
                if (vs[i]=="\n")
                {
                    vs[i] = " ";
                }
            }

            this.TopMost = true; //arkaya gecmeyi engelleme

            if (vs.Contains("Red"))
            {
                checkBox4.Checked = true;
            }
            if (vs.Contains("Blue"))
            {
                checkBox5.Checked = true;
            }
            if (vs.Contains("Black"))
            {
                checkBox6.Checked = true;
            }

            if (vs.Contains("Square"))
            {
                checkBox1.Checked = true;
            }
            if (vs.Contains("Triangle"))
            {
                checkBox2.Checked = true;
            }
            if (vs.Contains("Round"))
            {
                checkBox3.Checked = true;
            }

            if (vs.Contains("Easy"))
            {
                radioButton1.Checked = true;
            }
            if (vs.Contains("Medium"))
            {
                radioButton2.Checked = true;
            }
            if (vs.Contains("Hard"))
            {
                radioButton3.Checked = true;
            }
            if (vs.Contains("Custom"))
            {
                radioButton4.Checked = true;
                textBoxX.Text = x.ToString();
                textBoxY.Text = y.ToString();
            }
        }        

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox4.Checked)
            {
                colorRd = "Red";
            }
            else
                colorRd = string.Empty;

          
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox5.Checked)
            {
                colorBlue = "Blue";
            }
            else
                colorBlue = string.Empty;
           

        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox6.Checked)
            {
                colorBlack = "Black";
            }
            else
                colorBlack = string.Empty;
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                shapeSq = "Square";
            }
            else
                shapeSq = string.Empty;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            {
                shapeTr = "Triangle";
            }
            else
                shapeTr = string.Empty;
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked)
            {
                shapeRo = "Round";
            }
            else
                shapeRo = string.Empty;
        }
        
    }
}
