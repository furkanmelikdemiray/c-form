using System.Xml;
using System.Text;
using System.Data;
using System.Xml.Linq;
using System.Web;
using System.Data.SqlClient;

namespace FormTutorial
   
{
    public partial class Form1 : Form
    { 
        string[] vs = new string[] { };
        string AdminOrUser;
        bool isValid=false;
        public Form1()
        {
            InitializeComponent();            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            isAdminOrUser();
            if (AdminOrUser == "Admin")
            {
                AdminGameMenu adminGameMenu = new AdminGameMenu();
                adminGameMenu.Show();
            }
            if (textBox2.PasswordChar == '*')
            {
                textBox2.PasswordChar = '\0';
            }
            UserToTxt();            
            try
            {
                isUserNameAndPswValid();
                
                if (string.IsNullOrEmpty(textBox1.Text) || string.IsNullOrEmpty(textBox2.Text))
                {
                     throw new Exception("You don't enter Username or Password!");
                }
            }
            catch (Exception hata)
            {
                MessageBox.Show("Error: " + hata.Message);
                return;
            }
        }
        void isUserNameAndPswValid()
        {            
            string UserName, Password;
            XmlDocument doc = new XmlDocument();
            Dictionary<string, string> map = new Dictionary<string, string>();
            if (File.Exists("Users.xml") && AdminOrUser=="User")
            {
                doc.Load("Users.xml");
                foreach (XmlNode node in doc.DocumentElement)
                {
                    UserName = node["UserName"].InnerText;
                    Password = node["Password"].InnerText;
                    map.Add(UserName, Password);
                }
            }               
            foreach (KeyValuePair<string,string> key in map)
            {
                if (key.Key == textBox1.Text && key.Value == textBox2.Text)// buray� sha2 yap
                {
                    GameMenu gameMenu = new GameMenu();
                    gameMenu.Show();
                    isValid = true;
                }
            }
            

        }
        void isAdminOrUser()
        {
            //if (textBox1.Text=="Admin" && textBox2.Text=="Admin" || textBox1.Text == "admin" && textBox2.Text == "admin"||
            //    textBox1.Text == "admin" && textBox2.Text == "Admin" || textBox1.Text == "Admin" && textBox2.Text == "admin")
            //{
            //    AdminOrUser = "Admin";
            //}
            //else
            //{
            //    AdminOrUser = "User";
            //}

            if (textBox1.Text == "Admin" && textBox2.Text == "admin")
            {
                AdminOrUser = "Admin";
            }
            else AdminOrUser = "User";
        }
        void UserToTxt()
        {
            TextWriter textWriter = new StreamWriter("./usersave.txt", false);
            textWriter.WriteLine(textBox1.Text);
            textWriter.WriteLine(textBox2.Text);
            textWriter.Close();
        }        
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(textBox1.Text, "^[a-zA-Z ]"))
            {
                MessageBox.Show("This textbox accepts only alphabetical characters");
                //textBox1.Text.Remove(textBox1.Text.Length - 1);
            }
        }

        private void AdminRadio_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {            
            isAdminOrUser();
            if (AdminOrUser=="Admin")
            {
                AdminSignUp adminSignUp = new AdminSignUp();
                adminSignUp.Show();
            }
            else if(AdminOrUser== "User")
            {
                UserSetting userSetting = new UserSetting();
                userSetting.Show();
            }            
        }

        private void Form1_Load(object sender, EventArgs e)
        {           
            this.ActiveControl = textBox1;
            if (File.Exists("./save.txt"))
            {
                string words = File.ReadAllText("./save.txt");
                vs = words.Split(" ");
            }
            else
            {
                TextWriter txt = new StreamWriter("./save.txt", false);
                txt.Close();
            }

            for (int i = 0; i < vs.Length; i++)
            {
                if (vs[i] == "\n")
                {
                    vs[i] = " ";
                }
            }
            if (File.Exists("usersave.txt"))
            {
                StreamReader streamReader = new StreamReader("usersave.txt");
                textBox1.Text = streamReader.ReadLine();
                streamReader.Close();
            }
            else
            {
                TextWriter usertxt = new StreamWriter("usersave.txt", false);
                usertxt.Close();
            }
        }
        private void bindingSource1_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void UserType_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox1.Checked == false)
            {
                textBox2.PasswordChar = (char)0;
            }
            else
            {
                textBox2.PasswordChar = '*';
            }
        }
    }
}