﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;

namespace FormTutorial
{
    public partial class Multiplayer : Form
    {      
        public Multiplayer()
        {
            InitializeComponent();           
        }

        private void Multiplayer_Load(object sender, EventArgs e)
        {
            
        }

        private void Connect_Click(object sender, EventArgs e)
        {
            MultiplayerGameForm newGame = new MultiplayerGameForm(false,IpBox.Text);
            try
            {
                Visible = false;
                newGame.ShowDialog();
                Visible = true;
            }
            catch (Exception hata)
            {
                MessageBox.Show(hata.Message);
            }
        }

        private void HostGame_Click(object sender, EventArgs e)
        {
            MultiplayerGameForm newGame = new MultiplayerGameForm(true);
            Visible = false;

            if (!newGame.IsDisposed)
            {
                label2.Text = "Connected";
                label2.ForeColor = Color.Green;
                newGame.ShowDialog();
            }

            Visible = true;

        }

    }
}
