﻿namespace FormTutorial
{
    partial class AdminSignUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UserNameTxtBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.NameTxtBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.AdressTxtBox = new System.Windows.Forms.RichTextBox();
            this.PhoneTxtBox = new System.Windows.Forms.MaskedTextBox();
            this.CityTxtBox = new System.Windows.Forms.TextBox();
            this.CountryTxtBox = new System.Windows.Forms.TextBox();
            this.EmailTxtBox = new System.Windows.Forms.TextBox();
            this.PasswordTxtBox = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // UserNameTxtBox
            // 
            this.UserNameTxtBox.Location = new System.Drawing.Point(324, -37);
            this.UserNameTxtBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UserNameTxtBox.Name = "UserNameTxtBox";
            this.UserNameTxtBox.Size = new System.Drawing.Size(275, 27);
            this.UserNameTxtBox.TabIndex = 27;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(236, -33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 20);
            this.label1.TabIndex = 19;
            this.label1.Text = "Username";
            // 
            // NameTxtBox
            // 
            this.NameTxtBox.Location = new System.Drawing.Point(135, 110);
            this.NameTxtBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NameTxtBox.Name = "NameTxtBox";
            this.NameTxtBox.Size = new System.Drawing.Size(275, 27);
            this.NameTxtBox.TabIndex = 44;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(192, 504);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(119, 39);
            this.button1.TabIndex = 43;
            this.button1.Text = "Register";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // AdressTxtBox
            // 
            this.AdressTxtBox.Location = new System.Drawing.Point(135, 199);
            this.AdressTxtBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.AdressTxtBox.Name = "AdressTxtBox";
            this.AdressTxtBox.Size = new System.Drawing.Size(275, 121);
            this.AdressTxtBox.TabIndex = 42;
            this.AdressTxtBox.Text = "";
            // 
            // PhoneTxtBox
            // 
            this.PhoneTxtBox.Location = new System.Drawing.Point(135, 154);
            this.PhoneTxtBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PhoneTxtBox.Mask = "(999) 000-0000";
            this.PhoneTxtBox.Name = "PhoneTxtBox";
            this.PhoneTxtBox.Size = new System.Drawing.Size(275, 27);
            this.PhoneTxtBox.TabIndex = 41;
            // 
            // CityTxtBox
            // 
            this.CityTxtBox.Location = new System.Drawing.Point(135, 337);
            this.CityTxtBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CityTxtBox.Name = "CityTxtBox";
            this.CityTxtBox.Size = new System.Drawing.Size(275, 27);
            this.CityTxtBox.TabIndex = 40;
            // 
            // CountryTxtBox
            // 
            this.CountryTxtBox.Location = new System.Drawing.Point(135, 382);
            this.CountryTxtBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CountryTxtBox.Name = "CountryTxtBox";
            this.CountryTxtBox.Size = new System.Drawing.Size(275, 27);
            this.CountryTxtBox.TabIndex = 39;
            // 
            // EmailTxtBox
            // 
            this.EmailTxtBox.Location = new System.Drawing.Point(135, 427);
            this.EmailTxtBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.EmailTxtBox.Name = "EmailTxtBox";
            this.EmailTxtBox.Size = new System.Drawing.Size(275, 27);
            this.EmailTxtBox.TabIndex = 38;
            // 
            // PasswordTxtBox
            // 
            this.PasswordTxtBox.Location = new System.Drawing.Point(135, 63);
            this.PasswordTxtBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PasswordTxtBox.Name = "PasswordTxtBox";
            this.PasswordTxtBox.Size = new System.Drawing.Size(275, 27);
            this.PasswordTxtBox.TabIndex = 37;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(135, 18);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(275, 27);
            this.textBox1.TabIndex = 36;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(69, 431);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 20);
            this.label8.TabIndex = 35;
            this.label8.Text = "E-mail";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(59, 386);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 20);
            this.label7.TabIndex = 34;
            this.label7.Text = "Country";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(84, 341);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 20);
            this.label6.TabIndex = 33;
            this.label6.Text = "City";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(60, 201);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 20);
            this.label5.TabIndex = 32;
            this.label5.Text = "Address";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 20);
            this.label4.TabIndex = 31;
            this.label4.Text = "Phone Number";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 20);
            this.label3.TabIndex = 30;
            this.label3.Text = "Name-Surname";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 20);
            this.label2.TabIndex = 29;
            this.label2.Text = "Password";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(47, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 20);
            this.label9.TabIndex = 28;
            this.label9.Text = "Username";
            // 
            // AdminSignUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 556);
            this.Controls.Add(this.NameTxtBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.AdressTxtBox);
            this.Controls.Add(this.PhoneTxtBox);
            this.Controls.Add(this.CityTxtBox);
            this.Controls.Add(this.CountryTxtBox);
            this.Controls.Add(this.EmailTxtBox);
            this.Controls.Add(this.PasswordTxtBox);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.UserNameTxtBox);
            this.Controls.Add(this.label1);
            this.Name = "AdminSignUp";
            this.Text = "AdminSignUp";
            this.Load += new System.EventHandler(this.AdminSignUp_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private TextBox UserNameTxtBox;
        private Label label1;
        private TextBox NameTxtBox;
        private Button button1;
        private RichTextBox AdressTxtBox;
        private MaskedTextBox PhoneTxtBox;
        private TextBox CityTxtBox;
        private TextBox CountryTxtBox;
        private TextBox EmailTxtBox;
        private TextBox PasswordTxtBox;
        private TextBox textBox1;
        private Label label8;
        private Label label7;
        private Label label6;
        private Label label5;
        private Label label4;
        private Label label3;
        private Label label2;
        private Label label9;
    }
}