﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace FormTutorial
{
    public partial class AdminSignUp : Form
    {
        public AdminSignUp()
        {
            InitializeComponent();
        }
       
        private void button1_Click_1(object sender, EventArgs e)
        {
            WriteXmlAdmins();
        }
        void WriteXmlAdmins()
        {
            XDocument xDocument = XDocument.Load(@"Admins.xml");
            XElement rootElement = xDocument.Root;
            XElement element = new XElement("Admin");
            XElement UserName = new XElement("UserName", textBox1.Text);
            XElement Password = new XElement("Password", PasswordTxtBox.Text);
            XElement NameSurname = new XElement("NameSurname", NameTxtBox.Text);
            XElement PhoneNumber = new XElement("Number", PhoneTxtBox.Text);
            XElement Address = new XElement("Adress", AdressTxtBox.Text);
            XElement City = new XElement("City", CityTxtBox.Text);
            XElement Country = new XElement("Country", CountryTxtBox.Text);
            XElement Email = new XElement("E-mail", EmailTxtBox.Text);
            element.Add(UserName, Password, NameSurname, PhoneNumber, Address, City, Country, Email);
            rootElement.Add(element);            
            xDocument.Save(@"Admins.xml");
            MessageBox.Show("Sign Up is complete");
        }
        void CreateXmlAdmin()
        {
            XmlTextWriter xmlTextWriter = new XmlTextWriter(@"Admins.xml", Encoding.UTF8);
            xmlTextWriter.Formatting = Formatting.Indented;
            xmlTextWriter.WriteStartDocument();
            xmlTextWriter.WriteStartElement("Admins");
            xmlTextWriter.WriteEndElement();
            xmlTextWriter.Close();
        }

        private void AdminSignUp_Load(object sender, EventArgs e)
        {
            if (File.Exists(@"Admins.xml"))
            {

            }
            else
                CreateXmlAdmin();
        }
    }
}

